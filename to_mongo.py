import pandas as pd
from pymongo import MongoClient

def dfToMongo(client, db, col, df):
    df.reset_index(inplace=True)
    data_dict = df.to_dict("records")
    col.insert_many(data_dict)

def importDataframe(host, port, dbName, colName, df):
    client = MongoClient(host, port)
    db = client[dbName]
    col = db[colName]
    dfToMongo(client, db, col, df)
