import os
import tarfile
import pandas as pd
import shutil
from pathlib import Path
import time as t
from to_mongo import *

def scanDirs(filepathIn, verbose=False, dirsList=[]) :
    subDirsList = []
    for (thisDir, subDirs, files) in os.walk(filepathIn):
        subDirsList.extend(subDirs)
    dirsList.extend(filepathIn+subDir for subDir in subDirsList)
    if (verbose) :
        print(dirsList)
    return dirsList

def wait(sec):
    print("\nWaiting %f seconds...\r" % sec)
    for i in range(sec):
        print(i+1,end='\r')
        t.sleep(1)
    print("Done.\n",end='\r')

def createDir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def rmDir(path):
    contenu=os.listdir(path)
    for x in contenu:
        pathX = os.path.join(path,x)
        os.remove(pathX) #on supprime tous les fichier dans le dossier
    os.rmdir(path) #puis on supprime le dossier

def unzip_files(tarList, path, pathDec):
    createDir(pathDec)
    for tarName in tarList:
        tarPath = os.path.join(path,tarName)
        tar = tarfile.open(tarPath)
        tar.extractall(pathDec)
        tar.close()

def readFile(fileList, pathDec):
    for file in fileList:
        filePath = os.path.join(pathDec, file)
        df = pd.read_csv(filePath, header=0, names=['id','temperature','humidity'])
        print(df.head())
        try:
            importDataframe("localhost", 27017, "multi_threading_db", "data", df)
        except:
            print("Error in Mongo insert of file " + filePath)

def getRawOutFolderPath(path, out):
    projectPath = os.path.dirname(os.path.dirname(path))
    return os.path.join(projectPath, out[:-1])

def moveFolder(path, tarList, out):
    rawOutPath = getRawOutFolderPath(path, out)

    # Get the new folder path with the sensor name
    folderPath = os.path.join(rawOutPath, os.path.basename(path))

    # Check if path already exists
    if os.path.isdir(folderPath) :
        # If folder exist move file one by one in dest folder 
        for tar in tarList: 
            shutil.move(path + "/" + tar, folderPath)
    else : 
        # If folder not exist move entire folder to dest folder
        shutil.move(path, folderPath)
        


def process_file(path, out):
    decFolder = 'dec'
    pathDec = os.path.join(path, decFolder)

    tarList = [ tgz for tgz in os.listdir(path) if os.path.isfile(os.path.join(path,tgz)) ]
    unzip_files(tarList, path, pathDec)
    
    fileList = [ f for f in os.listdir(pathDec) if os.path.isfile(os.path.join(pathDec,f)) ]
    readFile(fileList, pathDec)

    rmDir(pathDec)
    
    moveFolder(path, tarList, out)

