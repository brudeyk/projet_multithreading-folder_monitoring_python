# [![Git Logo](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Git_icon.svg/52px-Git_icon.svg.png) Git du projet_multithreading-folder_monitoring_python](https://gitlab.com/brudeyk/projet_multithreading-folder_monitoring_python)

## Projet de multithreading - **Sujet 4** - DE3 - Télécom Saint-Etienne - 2021

### *Kent B., Gabrielle F., Arthur B.*

Implement a Python project that monitors a directory and processes all files found in this directory.

A lots of files are produced in this directory : CSV files containing temperature measure for a given sensor.

The file name follows this structure _`<sensorId>`_/_`<YYYYMMDDHHmmssSSS>`_.tgz

The files contain lines with this structure : timestamp,temperature,humidity

ex : 20210305142905126,11.22,84.12

<br>

#### Dépendances externes

- `pip install pymongo`
- `pip install pandas`

<br>

#### Génération des fichiers bruts

Script `generate_tgz_per_sensor.py` qui génère un nombre donné/aléatoire d'archives _.tgz_ contenant un nombre aléatoire de _.csv_ avec un nombre de lignes aléatoire ; pour un `sensorId` donné/aléatoire. Quelques exemplaires générés dans `raw_in/`.

`python3` `generate_tgz_per_sensor.py` _`<sensorId>`_ _`<nbArchive>`_

- `<sensorId>` : 1er argument à définir, sinon nombre aléatoire entre 0 et 99
- `<nbArchive>` : 2e argument à définir, sinon nombre aléatoire entre 1 et 10

<br>

#### Paramètres variables du programme principal

- `filepathIn` : dossier de provenance des données brutes, par défaut `filepathIn = "raw_in/"`
- `filepathOut` : dossier de sortie avant archivage des données brutes, par défaut `filepathOut = "raw_out/"`
- `filepathZip` : dossier de sortie des données archivées, par défaut `filepathZip = "archive/"`
- `delay` : (en secondes) temps d'attente entre chaque lecture de dossier, par défaut `delay = 10.0`
- `ntimes` : nombre minimum d'itération de lecture de dossier avant archivage, par défaut `ntimes = 3`

<br>

#### Consignes

The program has to : 

- periodically launch threads to scan for input directory, 
- process each file found in the directory for populating a database 
- move the processed files to another directory
- periodically realize a zip with the processed files to archive them
- delete the file once zipped in archive

<br>

#### Workflow de l'application

<br>

Toutes les 30 minutes :  
- 	scan les dossiers capteurs  
-   mettre tous les paths dans un tableau  
-   compter les dossiers  
- 	lancer 4 threads se repartis en fonction du nombre de dossiers :  
		-   Algo thread :  
            -   Pour chaque dossier :  
                -   Pour chaque tgz du dossier :  
                    -   Décompresser le tgz  
                    -   Pour chaque csv décompressés :  
                        -   Lire les données  
                        -   Mettre en base de données les mesures  
                        -   Supprimer le csv  
                    -   Fin Pour  
                    -   Déplacer le tgz vers un dossier "mesures traitées" (les dossiers de mesures traitées on la même arbo que les dossier de mesures à traiter)  
                -   Fin Pour  
            -   Fin Pour  

Tous les jours :  
	
-   Lancer un thread :  
        -   Algo thread :  
            -   Compresser les dossiers "mesures traitées" en une unique archive nommée \<timestamp\>.zip  
            -   Envoyer l'archive dans un dossier archive  
            -   Vider chaque dossier de mesures traitées  

<br>

#### Illustration du workflow multithreading

<br>

![Illustration du workflow multithreading](doc/multithreading-python.png)

<br>

#### Sources

- [Thread delay](https://stackoverflow.com/questions/15795404/how-to-make-an-action-happen-every-minute-in-python)
- [Multithreading in python](https://analyticsindiamag.com/how-to-run-python-code-concurrently-using-multithreading/)