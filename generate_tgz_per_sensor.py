# Random pour simuler l'aléatoire
import random
# OS pour modifier le système de fichier
import os
# Datetime pour la génération automatique des noms de fichiers
import datetime as dt
# Pandas pour faciliter la génération en csv
import pandas as pd

import glob
import tarfile

import sys

random.seed(6)

#################################FONCTIONS
def rmDir(path):
    contenu=os.listdir(path)
    for x in contenu:
        os.remove("%s%s" % (path,x)) #on supprime tous les fichier dans le dossier
    os.rmdir(path) #puis on supprime le dossier

def createDir(path):
    if not os.path.exists(path):
        os.makedirs(path)
def okay():
    print("\nDone.\n")
##########################################

filepathIn = "raw_in/"
filepathOut = "raw_out/"
now = dt.datetime.now()
datm = "%4d%02d%02d%02d%02d%02d%003d" % (now.year,now.month,now.day,now.hour,now.minute,now.second,int("0%s" % str(now.microsecond)[:-3]))
print("%s : format YYYYMMDDHHmmssSSS" % datm)

if len(sys.argv)>=2:
    print(len(sys.argv))
    if(sys.argv[1]) :
        sensorId = int(sys.argv[1])
else :
    sensorId = random.randint(0,99)

if len(sys.argv)>=3:
    if(sys.argv[2]) :
        nbArchives = int(sys.argv[2])
else :
    nbArchives = random.randint(1,10)

random.seed(sensorId)
minT = -10
maxT = 40
temperature = minT + (maxT-minT)*random.random()
minH = 0
maxH = 99
humidity = minH + (maxH-minH)*random.random()
filepathInSensor = '{}{}/'.format(filepathIn,sensorId)

for v in range (nbArchives):  
    now = dt.datetime.now()
    archiveName = "%4d%02d%02d%02d%02d%02d%003d" % (now.year,now.month,now.day,now.hour,now.minute,now.second,int("0%s" % str(now.microsecond)[:-3]))
    filepathInArchive = '{}{}/'.format(filepathInSensor,archiveName)
    createDir(filepathInArchive)
    nbFiles = random.randint(1,30)
    nbLines = random.randint(2,10)
    for n in range (nbFiles):
        mes = pd.DataFrame(columns=['timestamp','temperature','humidity'])
        now = dt.datetime.now()
        filename = "%4d%02d%02d%02d%02d%02d%003d.%d.csv" % (now.year,now.month,now.day,now.hour,now.minute,now.second,int("0%s" % str(now.microsecond)[:-3]), sensorId)
        print("Creating %s (%d/%d) and writing %d lines..." % (filename,n+1,nbFiles,nbLines))
        f = open('{}{}'.format(filepathInArchive,filename), "w")
        for i in range(nbLines):
            now = dt.datetime.now()
            f.write("%4d%02d%02d%02d%02d%02d%003d,%.2f,%.2f\n" % (now.year,now.month,now.day,now.hour,now.minute,now.second,int("0%s" % str(now.microsecond)[:-3]), minT + (maxT-minT)*random.random(), minH + (maxH-minH)*random.random()))
        f.close()
        okay()
    tar = tarfile.open("%s%s.tgz" % (filepathInSensor,archiveName), "w:gz")
    for fileName in glob.glob(os.path.join(filepathInArchive, "*csv")):
        print("\n  Adding %s..." % fileName )
        tar.add(fileName, os.path.basename(fileName))
    print("\n  ...To %s%s.tgz\n" % (filepathInSensor,archiveName))
    tar.close()
    print("Removing %s folder..." % filepathInArchive)
    rmDir(filepathInArchive) 
    okay()
