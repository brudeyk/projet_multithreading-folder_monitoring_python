from process_file import *
import shutil
import os, re, os.path
import datetime as dt
import time as t

def toZip(srcFolder, dstFolder):
    archiveName = dt.datetime.now().strftime('%Y%m%d%I%M%S')
    shutil.make_archive(dstFolder + "/" + archiveName, 'zip', srcFolder)

def empty(folder):
    for root, directories, files in os.walk(folder):
        for file in files:
            os.remove(os.path.join(root, file))

def archive(srcFolder, dstFolder):
    start = t.perf_counter()
    createDir(dstFolder)
    toZip(srcFolder, dstFolder)
    empty(srcFolder)
    finish = t.perf_counter()
    print(f'Finished archiving file in {round(finish-start, 2)} seconds')
