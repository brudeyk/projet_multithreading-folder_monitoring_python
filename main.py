from process_file import *
from archive import *
import requests
import time as t
import threading
import concurrent.futures
# Datetime pour la génération automatique des noms de fichiers
import datetime as dt
# OS pour modifier le système de fichier
import os

filepathIn = "raw_in/"
filepathOut = "raw_out/"
filepathZip = "archive/"
delay = 10.0
ntimes = 3

dirsList=[]
dirsList = scanDirs(filepathIn, verbose=True, dirsList=dirsList)

i = 0
while(1):
    i += 1
    now = dt.datetime.now()
    datm = "%4d%02d%02d%02d%02d%02d%003d" % (now.year,now.month,now.day,now.hour,now.minute,now.second,int(str(now.microsecond)[:-3]))

    # start = t.perf_counter()
    # th = threading.Thread(target=wait, args=[60])
    # th.start()
    # th.join()
    # finish = t.perf_counter()
    # print(f'\nFinished in {round(finish-start, 2)} seconds\n') 
  
    start = t.perf_counter()
    args = ((directory, filepathOut) for directory in dirsList)
    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(lambda p: process_file(*p), args)
    finish = t.perf_counter()
    print(f'Finished reading files in {round(finish-start, 2)} seconds') 

    if i >= ntimes :
        i = 0
        T1 = threading.Timer(delay*ntimes, archive, args=(filepathOut,filepathZip))
        T1.start()
        # T1.join()
        # with concurrent.futures.ThreadPoolExecutor() as executor:
        #     executor.submit(archive, filepathOut, filepathZip)

    dirsList=[]

    T2 = threading.Timer(delay, scanDirs, args=(filepathIn, False, dirsList))

    T2.start()

    #print(dirsList)

    T2.join()

    print(dirsList)